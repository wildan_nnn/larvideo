@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!

                    <div class="content">
                        <div class="m-b-md" style="align-items: center;
                                                    display: flex;
                                                    justify-content: center;">
                            <video id="my-video" class="video-js" controls preload="auto" width="400" height="264" poster="" data-setup="{}">
                              <source src="gambar/v1.mp4" type='video/mp4'>
                              <p class="vjs-no-js">
                                To view this video please enable JavaScript, and consider upgrading to a web browser that
                                <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                              </p>
                              <div class="vjs-text-track-display"></div>
                              <div class="vjs-loading-spinner"></div>
                            </video>
                            <img src="gambar/cctv2.jpg" name="slide" width="400" height="300">
                            <!-- <div id="gambar"></div> -->
                        </div>

                        <div class="cam-viewer-action">
                            <div class="btn-group">
                                <a href="#" id="refresh" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i> Refresh</a>
                                <a href="#" class="btv btn btn-default btn-sm"><i class="fa fa-youtube-play"></i> Video</a>
                                <a href="#" class="bti btn btn-default btn-sm"><i class="fa fa-picture-o"></i> Image</a>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-bar-chart"></i> Statistic</a>
                                <i class="btnm">
                                <a href="#" class="bts btn btn-default btn-sm"><i class="fa fa-volume-off"></i> Mute</a>
                                <a href="#" class="btd btn btn-default btn-sm"><i class="fa fa-volume-up"></i> Unmute</a>
                                </i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">      
      var image1 = new Image();
      image1.src = "gambar/cctv1.jpg";

      var image2 = new Image();
      image2.src = "gambar/cctv2.jpg";

      var image3 = new Image();
      image3.src = "gambar/cctv3.jpg";

      var image4 = new Image();
      image4.src = "gambar/cctv4.jpg";

      var image5 = new Image();
      image5.src = "gambar/cctv5.jpg";

      var image6 = new Image();
      image6.src = "gambar/cctv6.jpg";

      var image7 = new Image();
      image7.src = "gambar/cctv7.jpg";

      var image8 = new Image();
      image8.src = "gambar/cctv8.jpg";

      var image9 = new Image();
      image9.src = "gambar/cctv9.jpg";

      var image10 = new Image();
      image10.src = "gambar/cctv10.jpg";

      var step=1;
      function gerak(){
        if(!document.images) {
            return 
        } else {
            document.images.slide.src = eval("image"+step+".src");
        }

        if(step < 10)
            step++;
        else
            step=1;
            setTimeout("gerak()",1000);
      }
      gerak();

    $(document).ready(function(){

      $("#my-video").hide();
      $(".bti").hide();
      $(".btd").hide();
      $(".btnm").hide();

      // function myfungsi(){
      //   // gambar pertama
      //   var myImage = new Image(400, 264);
      //   myImage22.src = 'gambar/1.jpg';
      //   // gambar kedua
      //   var myImage2 = new Image(400, 264);
      //   myImage21.src = 'gambar/2.jpg';
      //   x = document.getElementById("gambar");

      //   s = setTimeout(function(){ 
      //   x.appendChild(myImage); 
      //   }, 1000);

      //   setTimeout(function(){ 
      //   x.appendChild(myImage2); 
      //   }, 1000);

      // }

      $("#refresh").click(function(){
        videojs("#my-video").ready(function(event) {
           var myPlayer = this;
           var myPlayer2 = this;
           myPlayer.load();
           myPlayer2.play();
        });
      });

      $(".btv").click(function(){
        $("#my-video").show();
        $("img").hide();
        $(".btv").hide();
        $(".bti").show();
        videojs("#my-video").ready(function(event){
           var myPlayer = this;
           myPlayer.play();
        });
        $(".btnm").show();
      });

      $(".bti").click(function(){
        $("img").show();
        $("#my-video").hide();
        $(".bti").hide();
        $(".btv").show();
        $(".btnm").hide();
      });

      $(".bts").click(function(){
        videojs("#my-video").ready(function(event){
           var myPlayer = this;
           var myPlayer2 = this;
           myPlayer.muted(true);
           $(".bts").hide();
           $(".btd").show();
        });
      });

      $(".btd").click(function(){
        videojs("#my-video").ready(function(event){
           var myPlayer = this;
           var myPlayer2 = this;
           myPlayer.muted(false);
           $(".btd").hide();
           $(".bts").show();
        });
      });

    });
</script>
@endsection
