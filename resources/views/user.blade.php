@extends('layouts.layout')

@section('title')
  Menu User
@endsection

@section('content')
<div class="container-fluid">
    <!-- CPU Usage -->
    <div class="row clearfix">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="body">
                  <h3>User <a href="javascript:void(0)" id="add" class="btn btn-primary waves-effect">Tambah</a></h3>
                  @if($m = Session::get('pesan'))
                  <div class="alert alert-block alert-warning">
                    <button type="button" class="close" data-dismiss="alert">
                      <i class="ace-icon fa fa-times"></i>
                    </button>

                    <em class="fa fa-lg fa-warning">&nbsp;</em> {{ $m }}
                  </div>
                  @elseif($ms = Session::get('pesanb'))
                  <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                      <i class="ace-icon fa fa-times"></i>
                    </button>

                    <em class="fa fa-lg fa-thumbs-up">&nbsp;</em> {{ $ms }}
                  </div>
                  @endif
                  <table class="table table-hover table-striped" id="data">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th class="col-md-2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($us as $user)
                      <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                          <b style="color: {{ ($user->status == 'login') ? 'green':'red' }}">
                            {{ ($user->status == 'login') ? 'Login':'Logout' }}
                          </b></td>
                        <td>
                          <form action="{{ route('del_us',$user->id) }}" method="POST" >
                            {{ csrf_field() }}
                            <a href="{{ route('edit_us',$user->id) }}" class="btn btn-warning waves-effect">Edit</a>
                            @if($user->status == 'login')
                            <button type="submit" class="btn btn-danger waves-effect disabled">Hapus</button>
                            @else
                            <button type="submit" class="btn btn-danger waves-effect" onclick="return confirm('Anda yakin akan menghapus?')">Hapus</button>
                            @endif
                          </form>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# CPU Usage -->
  </div>
@endsection

@section('js')
<script>
<!--
  $(document).ready(function() {
    $('#data').DataTable();
  } );

  $(document).ready(function(){
    $('#add').click(function(){
      window.location.href='./user/tambah';
    });
  });

  -->
</script>
@endsection
