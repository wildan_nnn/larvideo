@extends('layouts.layout')

@section('title')
  Menu User | Tambah User
@endsection

@section('content')
<div class="container-fluid">
  <div class="row clearfix">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="card">
        <div class="header">
          <div class="row clearfix">
            <div class="col-xs-12 col-sm-12">
              <h2>Tambah Data</h2>
            </div>
          </div>
        </div>
        <div class="body">
          <div class="row clearfix">
            <div class="col-sm-12">
              <form action="{{ route('store_us') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                  <div class="form-line">
                    <input type="text" class="form-control" name="nm" placeholder="Nama" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="form-line">
                    <input type="email" class="form-control" name="email" placeholder="Email" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="form-line">
                    <input type="password" class="form-control" name="pass" placeholder="Password" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="form-line">
                    <input type="password" class="form-control" name="konfi" placeholder="Confirm Password" />
                  </div>
                </div>
                <button type="submit" name="submit" class="btn btn-primary waves-effect">SIMPAN</button>
                <a href="{{ route('us') }}" class="btn btn-warning waves-effect">KEMBALI</a>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')

@endsection
